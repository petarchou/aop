package com.softuni.aop.basicexample;

import com.softuni.aop.Student;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name="examples.basic.enabled", havingValue = "true")
public class BasicExample implements CommandLineRunner {

    private final Student student;

    public BasicExample(Student student) {
        this.student = student;
    }

    @Override
    public void run(String... args) throws Exception {
        student.sayHello();
        student.sayGoodbye();
        try {
            student.suicide();
        } catch (Exception e) {

        }
    }
}
