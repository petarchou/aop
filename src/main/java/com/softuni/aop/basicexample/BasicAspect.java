package com.softuni.aop.basicexample;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Aspect
@Component
@ConditionalOnProperty(name="examples.basic.enabled", havingValue = "true")
public class BasicAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(BasicAspect.class);

    @Pointcut("execution(* com.softuni.aop.Student.*(..))")
    public void anyMethod() {}

    @Pointcut("execution(* com.softuni.aop.Student.sayGoodbye(..))")
    public void goodByeMethod() {}


    @Before("anyMethod()")
    public void logMethods(JoinPoint joinPoint) {
        LOGGER.info("Before calling: {}",joinPoint.getSignature());
    }
    @Before("goodByeMethod()")
    public void logGoodbye(JoinPoint joinPoint) {
        LOGGER.info("Before calling: {}",joinPoint.getSignature());
    }

    @AfterThrowing(pointcut = "anyMethod()", throwing = "error")
    public void logError(Throwable error) {
        LOGGER.info("Attempted suicide, calling 112..",error);
    }
}
