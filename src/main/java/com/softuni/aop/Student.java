package com.softuni.aop;

import org.springframework.stereotype.Component;

@Component
public class Student {

    public void sayHello() {
        System.out.println("Hello");
    }

    public void sayGoodbye() {
        System.out.println("Goodbye");
    }

    public String concat(String a, String b) {
        return a+b;
    }

    public void suicide(){
        throw new UnsupportedOperationException("Whoah, calm down");
    }
}
