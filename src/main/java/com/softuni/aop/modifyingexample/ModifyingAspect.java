package com.softuni.aop.modifyingexample;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Aspect
@Component
@ConditionalOnProperty(name = "examples.modifying.enabled", havingValue = "true")
public class ModifyingAspect {


    @Pointcut("execution(* com.softuni.aop.Student.concat(..))")
    public void concatMethod() {
    }

    ;


    @Around("concatMethod() && args(a,b)")
    public Object modifyMethod(ProceedingJoinPoint pjp, String a, String b) throws Throwable {
        Object result = pjp.proceed(new Object[]{"["+a+"]-", "["+b+"]"});
        return "("+result+")";  

    }
}
