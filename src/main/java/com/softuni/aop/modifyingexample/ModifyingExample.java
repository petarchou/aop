package com.softuni.aop.modifyingexample;

import com.softuni.aop.Student;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(name="examples.modifying.enabled",havingValue = "true")
public class ModifyingExample implements CommandLineRunner {
    private final Student student;

    public ModifyingExample(Student student) {
        this.student = student;
    }

    @Override
    public void run(String... args) throws Exception {

        System.out.println(student.concat("bana","na"));
    }
}
