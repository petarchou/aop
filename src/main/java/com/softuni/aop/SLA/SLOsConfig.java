package com.softuni.aop.SLA;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@ConfigurationProperties(prefix = "slos-config")
@Configuration
public class SLOsConfig {

    private List<SLO> slos = new ArrayList<>();

    public List<SLO> getSlos() {
        return slos;
    }

    public static class SLO {

        private String id;
        private int threshold;

        public SLO(String id, int threshold) {
            this.id = id;
            this.threshold = threshold;
        }

        public String getId() {
            return id;
        }

        public int getThreshold() {
            return threshold;
        }

        public SLO setId(String id) {
            this.id = id;
            return this;
        }

        public SLO setThreshold(int threshold) {
            this.threshold = threshold;
            return this;
        }
    }
}
