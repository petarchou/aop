package com.softuni.aop.SLA;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Aspect
@Component
public class SLOAspect {

    private static final Logger LOGGER = LoggerFactory.getLogger(SLOAspect.class);
    private final SLOsConfig slOsConfig;

    public SLOAspect(SLOsConfig slOsConfig) {
        this.slOsConfig = slOsConfig;
    }

    @Around("@annotation(TrackLatency)")
    public Object trackLatency(ProceedingJoinPoint pjp, TrackLatency TrackLatency) throws Throwable {
        String latencyId = TrackLatency.latency();
        SLOsConfig.SLO slo = slOsConfig.getSlos().stream()
                .filter(slo1 -> slo1.getId().equals(latencyId)).findFirst()
                .orElseThrow(() -> new IllegalStateException("SLO with ID " + latencyId + " is not configured."));

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Object result = pjp.proceed();
        stopWatch.stop();

        long actualTime = stopWatch.getLastTaskTimeMillis();
        long delay = actualTime - slo.getThreshold();
        if(delay > 0) {
            // basically we can do a lot of calculations here. For simplicity we log the sub-optimal request.
            LOGGER.warn("Method " + pjp.getSignature() + " executed with a delay of " + delay + " ms");
        }

        return result;
    }

}
