package com.softuni.aop.SLA;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DriverController {

    @TrackLatency(latency = "local_operation")
    @GetMapping("/drivers")
    public ResponseEntity<List<Driver>> getAllDrivers() {
        return ResponseEntity.ok(List.of(
                new Driver("Pesho", "B"),
                new Driver("Lacho", "A")
        ));
    }

    @TrackLatency(latency = "remote_operation")
    @GetMapping("/drivers/sync")
    public ResponseEntity<List<Driver>> syncDrivers() {

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        return ResponseEntity.ok(List.of(
                new Driver("Pesho", "B"),
                new Driver("Lacho", "A")
        ));

    }

}
